using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformerControllerInputL : MonoBehaviour {
    //---------------- Variables ----------------
    // Refer the OSC.cs file
    public OSC osc;

    // Set Vive controllers
    // reference to the tracked obj aka controller
    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    //---------------- Functions ----------------
    // Evaluates before start
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();

    }

    ...

    // Update is called once per frame
    void Update () {
        HandleInput();
        VelocityAccelaration();
        
    }
    
    ... 

    void HandleInput()
    {
        // OSC
        OscMessage oscMsgTrackpad = new OscMessage();

        // set osc address
        oscMsgTrackpad.address = "/ControllerLTrackpad";

        ...

        // This one already sends 0 if not touching
        oscMsgTrackpad.values.Add(Controller.GetAxis().y);

        // Send the message to the client
        osc.Send(oscMsgTrackpad);

    }

}
