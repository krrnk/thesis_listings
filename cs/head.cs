using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HMDSendOSC : MonoBehaviour {
	//---------- Variables ---------- 
    // Refer the OSC.cs file
    public OSC osc;

    //---------- Functions ---------- 
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {
        // Instantiate OSC message on very framerate 
        // so the array keeps a constant size
        OscMessage oscMsgPos = new OscMessage();
        OscMessage oscMsgRot = new OscMessage();

        // Set OSC address
        oscMsgPos.address = "/HMDPosition";
        oscMsgRot.address = "/HMDRotation";

        // Add the elements to the array
        oscMsgPos.values.Add(this.transform.position.x); 
        oscMsgPos.values.Add(this.transform.position.y); 
        oscMsgPos.values.Add(this.transform.position.z); 

        oscMsgRot.values.Add(this.transform.localRotation.eulerAngles.x); 
        oscMsgRot.values.Add(this.transform.localRotation.eulerAngles.y); 
        oscMsgRot.values.Add(this.transform.localRotation.eulerAngles.z);

        // Send the message to the client
        osc.Send(oscMsgPos);
        osc.Send(oscMsgRot);

    }
	
}
