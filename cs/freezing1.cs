using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformerControllerInputL : MonoBehaviour {
    //---------------- Variables ----------------
    // Refer the OSC.cs file
    public OSC osc;

    // Set Vive controllers
    // reference to the tracked obj aka controller
    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    // Access other ganme objects
    public GameObject floor;
    public GameObject ceiling;
    public GameObject wall1;
    public GameObject wall2;
    public GameObject wall3;
    public GameObject wall4;

    public GameObject theOtherController;

    public bool pressingGrip = false;
    public bool freezingFloor = false;
    public bool freezingCeiling = false;
    public bool freezingWall1 = false;
    public bool freezingWall2 = false;
    public bool freezingWall3 = false;
    public bool freezingWall4 = false;

    // Freeze OSC var
    float freeze = 0;

    //---------------- Functions ----------------
    // Evaluates before start
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();

    }

    // Update is called once per frame
    void Update () {

        HandleInput();
        VelocityAccelaration();
    }

    ... 

    void HandleInput()
    {
        // OSC
        OscMessage oscMsgFreezer = new OscMessage();

        // set osc address
        oscMsgFreezer.address = "/leftFreezer";

        // use physics engine to cast the ray and check if it has hit something
        RaycastHit hit;

        Vector3 point;
        if (Physics.Raycast(trackedObj.transform.position, trackedObj.transform.forward, out hit))
        {
            ...

            if (Controller.GetPress(SteamVR_Controller.ButtonMask.Grip))
            {
                pressingGrip = true;
            }
            else
            {
                pressingGrip = false;
            }

            if (Controller.GetPress(SteamVR_Controller.ButtonMask.Grip))
            {
                if (hit.collider.gameObject == floor)
                {
                    floor.GetComponent<MeshDeformer>().springForce = 0;

                    freezingFloor = true;
                }
                else
                {
                    freezingFloor = false;
                }

                if (hit.collider.gameObject == ceiling)
                {
                    ceiling.GetComponent<MeshDeformer>().springForce = 0;

                    freezingCeiling = true;
                }
                else
                {
                    freezingCeiling = false;
                }

                if (hit.collider.gameObject == wall1)
                {
                    wall1.GetComponent<MeshDeformer>().springForce = 0;

                    freezingWall1 = true;
                }
                else
                {
                    freezingWall1 = false;
                }2

                if (hit.collider.gameObject == wall2)
                {
                    wall2.GetComponent<MeshDeformer>().springForce = 0;

                    freezingWall2 = true;
                }
                else
                {
                    freezingWall2 = false;
                }

                if (hit.collider.gameObject == wall3)
                {
                    wall3.GetComponent<MeshDeformer>().springForce = 0;

                    freezingWall3 = true;
                }
                else
                {
                    freezingWall3 = false;
                }

                if (hit.collider.gameObject == wall4)
                {
                    wall4.GetComponent<MeshDeformer>().springForce = 0;

                    freezingWall4 = true;
                }
                else
                {
                    freezingWall4 = false;
                }

                freeze = 1;
            }
            else
            {
                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingFloor == false)
                {
                 
                    floor.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingCeiling == false)
                {
                    ceiling.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingWall1 == false)
                {
                    wall1.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingWall2 == false)
                {
                    wall2.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingWall3 == false)
                {
                    wall3.GetComponent<MeshDeformer>().springForce = 20;
                }

                if (theOtherController.GetComponent<MeshDeformerControllerInputR>().freezingWall4 == false)
                {
                    wall4.GetComponent<MeshDeformer>().springForce = 20;
                }

                freeze = 0;
            }

            // add the elements to the OSC array
            oscMsgFreezer.values.Add(freeze);
           
        }

        // Send the message to the client
        osc.Send(oscMsgFreezer);

    }

}
