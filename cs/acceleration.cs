using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformerControllerInputL : MonoBehaviour {
    //---------------- Variables ----------------
    // Refer the OSC.cs file
    public OSC osc;
    
    // Physics
    Vector3 controllerLastPostion;
    Vector3 velocity;
    Vector3 acceleration;
    
    //---------------- Functions ----------------
    // Update is called once per frame
    void Update () {
        VelocityAccelaration();
        
    }
    
    void VelocityAccelaration ()
    {
        velocity = (transform.position - controllerLastPostion) / Time.deltaTime;
        controllerLastPostion = transform.position;
        acceleration = velocity / (Mathf.Pow(Time.deltaTime, 2));
        
        // OSC
        OscMessage oscMsg = new OscMessage();

        // set osc address
        oscMsg.address = "/ControllerLVelAcc";

        // add 
        oscMsg.values.Add(velocity.x);
        oscMsg.values.Add(velocity.y);
        oscMsg.values.Add(velocity.z);
        oscMsg.values.Add(acceleration.x);
        oscMsg.values.Add(acceleration.y);
        oscMsg.values.Add(acceleration.z);

        // Send the message to the client
        osc.Send(oscMsg);

    }

}
