using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformerControllerInputL : MonoBehaviour {
    //---------------- Variables ----------------
    // Refer the OSC.cs file
    public OSC osc;

    // Set Vive controllers
    // reference to the tracked obj aka controller
    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    float force;
    public float forceOffset;

    // FFT
    float fft;

    //---------------- Functions ----------------
    // Evaluates before start
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Use this for initialization
    void Start () {
        // Declare OSC Address
        osc.SetAddressHandler("/fft1", OnReceiveFFT);
    }

   void OnReceiveFFT(OscMessage message)
    {
        fft = message.GetFloat(0);
        force = fft;
    }

    void HandleInput()
    {
        // use physics engine to cast the ray and check if it has hit something
        RaycastHit hit;

        Vector3 point;
        if (Physics.Raycast(trackedObj.transform.position, trackedObj.transform.forward, out hit))
        {
            MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();
            Renderer renderer = hit.collider.GetComponent<Renderer>();

            point = hit.point;

            if (deformer)
            {
                // add offsett so the vertices are always pushed into the surface
                point += hit.normal * forceOffset;

                // Deform the mesh
                deformer.AddDeformingForce(point, force);

            }
            
        }
    }

    // Update is called once per frame
    void Update () {
        HandleInput();
    }
    
}
