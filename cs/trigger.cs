using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformerControllerInputL : MonoBehaviour {
    //---------------- Variables ----------------
    // Refer the OSC.cs file
    public OSC osc;

    // Set Vive controllers
    // reference to the tracked obj aka controller
    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    ...

    //---------------- Functions ----------------
    // Evaluates before start
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();        
    }

    // Update is called once per frame
    void Update () {
        HandleInput();
    }

    void HandleInput()
    {
        // OSC
        ...
        OscMessage oscMsgTrigger = new OscMessage();

        // set osc address
        ...
        oscMsgTrigger.address = "/ControllerLTrigger";
  
        // Get trigger
        if (Controller.GetHairTrigger())
        {
            // Add the elements to the OSC array
            oscMsgTrigger.values.Add(1);
        }
        else
        {
            // Add the elements to the OSC  array
            oscMsgTrigger.values.Add(0);
        }

        ...

        // Send the message to the client
        osc.Send(oscMsgTrigger);
    }

    ...
}
