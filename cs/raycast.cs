using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformerControllerInputL : MonoBehaviour {
    //---------------- Variables ----------------
    // Refer the OSC.cs file
    public OSC osc;

    // Set Vive controllers
    // reference to the tracked obj aka controller
    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    //---------------- Functions ----------------
    // Evaluates before start
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Update is called once per frame
    void Update () {
        HandleInput();
    }

    void HandleInput()
    {
        // OSC settings
        OscMessage oscMsg = new OscMessage();
        oscMsg.address = "/rayCastLocationL";
  
        // Use physics engine to cast the ray and check if it has hit something
        RaycastHit hit;

        // Target's coordinates
        Vector3 point;

        // Raycasting
        if (Physics.Raycast(trackedObj.transform.position, trackedObj.transform.forward, out hit))
        {
            
            // Hit will return a Vector3 point when hitting a collider
            point = hit.point;

            // Add the coordinates to the OSC array
            oscMsg.values.Add(point.x);
            oscMsg.values.Add(point.y);
            oscMsg.values.Add(point.z);
            oscMsg.values.Add(hit.distance);

        } else 
        {
            // Initialise the OSC array in 0
            // so supercollider doesn't get confused
            // if the Raycast doesn't happen
            oscMsg.values.Add(0);
            oscMsg.values.Add(0);
            oscMsg.values.Add(0);
            oscMsg.values.Add(0);
        }

        // Send the message to the client
        osc.Send(oscMsg);

    }

}
                                             