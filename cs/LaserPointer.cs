﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPointer : MonoBehaviour {
    // Access the controller
    private SteamVR_TrackedObject trackedObj;

    // a reference to the Laser’s prefab
    public GameObject laserPrefab;
    // laser stores a reference to an instance of the laser
    private GameObject laser;
    // the transform component is stored for ease of use
    private Transform laserTransform;
    // this is the position where the laser hits
    private Vector3 hitPoint;

    // Variables for teleporting around 
    public Transform cameraRigTransform;
    public GameObject teleportReticlePrefab;
    private GameObject reticle;
    private Transform teleportReticleTransform;
    public Transform headTransform;
    public Vector3 teleportReticleOffset;
    public LayerMask teleportMask;  // mask to filter the areas on which teleports are allowed
    private bool shouldTeleport;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    private void Start()
    {
        // Spawn a new laser and save a reference to it in laser
        laser = Instantiate(laserPrefab);
        // Store the laser’s transform component.
        laserTransform = laser.transform;

        // Spawn new recticle and save a reference in reticle
        reticle = Instantiate(teleportReticlePrefab);
        teleportReticleTransform = reticle.transform;
    }

    // Update is called once per frame
    void Update () {
        
        // If the touchpad is held down
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
        {
            RaycastHit hit;

            // Shoot a ray from the controller. If it hits something, make it store the point where it hit and show the laser
            if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100, teleportMask))
            {
                hitPoint = hit.point;
                ShowLaser(hit);
                // show the recticle
                reticle.SetActive(true);
                // move the recticle to where the raycast hit with the addition of an offset to avoid Z-fighting
                teleportReticleTransform.position = hitPoint + teleportReticleOffset;
                // set shouldTeleport to true to indicate the script found a valid position for teleporting
                shouldTeleport = true;

            }
        }
        else // Hide the laser when the player released the touchpad
        {
            laser.SetActive(false);
            reticle.SetActive(false);
        }

        // Teleport the player if the touchpad is released and there’s a valid teleport position
        if (Controller.GetHairTriggerUp() && shouldTeleport)
        {
            Teleport();
        }
    }

    // Laser
    private void ShowLaser(RaycastHit hit)
    {
        // Show the laser
        laser.SetActive(true);
        // Position the laser between the controller and the point where the raycast hits
        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f);
        // Point the laser at the position where the raycast hit
        laserTransform.LookAt(hitPoint);
        // Scale the laser so it fits perfectly between the two positions
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
            hit.distance);
    }

    // Teleporting
    private void Teleport()
    {
        shouldTeleport = false;
        reticle.SetActive(false);
        // calculate the difference between the positions of the camera rig’s center and the player’s head
        Vector3 difference = cameraRigTransform.position - headTransform.position;
        // reset the y-position for the above difference to 0, because the calculation doesn’t consider the vertical position of the player’s head
        difference.y = 0;
        // vove the camera rig to the position of the hit point and add the calculated difference. Without the difference, the player would teleport to an incorrect location
        cameraRigTransform.position = hitPoint + difference;
    }
}
