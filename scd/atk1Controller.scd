OSCdef(\rayCastLocationL, {
	|msg|
	
	var axisX, axisY, axisZ, radial, azimuth, polar, rayDistance, distance;
	
	// From cartesian coordinates to spherical
	// front and rear
	axisX = msg[3]; // Z transform.position axis in Unity
	
	// left and right
	axisY = msg[1]; // X transform.position axis in unity
	
	// top to bottom
	axisZ = msg[2]; // Y transform.position axis in unity
	
	// distance
	rayDistance = msg[4]; // RayCastHit hit.distance in Unity

	// Spherical coordinates formulas
	// radial sqrt(x^2 + y^2 + z^2)
	radial = sqrt(((axisX - ~hmdX) ** 2) + ((axisY - ~hmdY) ** 2) + ((axisZ - ~hmdZ) ** 2));
	// I'm working with Unity dimension of 50 (front-rear), 50 (left-right) and 25 (top-bottom)
	// Therefore my maximum value is 56.789083458003
	radial = radial.linlin(0, 56.789083458003, 0, pi / 2); // scale to radians
	
	// azimuth atan2(y, x)
	azimuth = atan2(axisY - ~hmdY, axisX - ~hmdX) * -1; // result will be in radians
	azimuth = azimuth - ~hmdYaw;

	// polar cos^(-1)(z/r)
	polar = axisZ.linlin(-28.92, 28.92, -pi / 2, pi / 2); // scale to radians
	polar = polar - ~hmdPitch;
	
	// Distance
	distance = rayDistance.linlin(0, 50, 0.1, 5);
},
'/rayCastLocationL', // message address
nil,
6161 // message port
);