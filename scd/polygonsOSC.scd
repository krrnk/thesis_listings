{
	var polygonsRotating = false;
	
	var leftTrackpadOn = false;
	var rightTrackpadOn = false;
	
	OSCdef(\environmentReacts, {
		var accAvg;
		
		accAvg = (accelerationL + accelerationR) / 2;
		
		...
		
		// polygons Counter
		if(accAvg < 250){
			
			if(polygonsCounterResets == false){
				...
			};
			
			
		} // else
		{
			if(polygonsCounterPlays == false){
				...
			};
			
		};
		
		// Events on the polygons
		if((polygonsCounter >= 4) && ((leftTrackpadOn == true) || (rightTrackpadOn == true))){
			if(polygonsPlay == false){
				
				...
				
			};
			
			if(polygonsCounter >= 4){
				client.sendMsg("/polygonsRotation", 0, polygonRotY, 0);
				
				// Adjust rotation speed depending on user input
				if(accAvg > 250){
					
					s.sendMsg(\n_set, 1054, \rate1, [-0.2, 0.2].choose, \rate2, rrand(0.2, 0.25), \rate3, [-0.2, 0.2].choose);
					
					client.sendMsg("/polygonsRotation", polygonRotX, polygonRotY, polygonRotZ);
					
					polygonsRotating = true;
					
				} // else
				{
					if((accAvg > 100) && (accAvg < 250)){
						s.sendMsg(\n_set, 1054, \rate2, rrand(0.4, 0.6));
					} // else
					{
						s.sendMsg(\n_set, 1054, \rate2, rrand(0.08, 0.1));
					};
					
					client.sendMsg("/polygonsRotation", 0, polygonRotY, 0);
					polygonsRotating = true;
					
				};
				
			} // else
			{
				client.sendMsg("/polygonsRotation", 0, 0, 0);
				
				polygonsRotating = false;
				
			};
			
			
		} // else
		{
			polygonsRotating = false;
			
			if(polygonsReset == false){
				
				if(environmentCameraCounter > 10){
					
					...
					
					polygonsRotating = false;
					
					
				};
				
			};
			
		};
	},
	'/tr',
	s.addr
	);
	
};